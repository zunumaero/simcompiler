function CompileModel(model_dir, model, regenModel)
% Compile the simulation model into a wrapper executable

clc
grt_rtw = '_grt_rtw/';
origDir = pwd;
cd(model_dir);


%% Build Simulink model

evalin('base', 'Init');
load_system(model);

% Set all logged signals to ExportedGlobal
disp('Setting external storage class for logged signals...');
simSignals = get_param(model, 'InstrumentedSignals');
N = simSignals.Count;
for i=1:N
    signal = get(simSignals,i);
    blockPath = signal.BlockPath.getBlock(1);
    pH = get_param(blockPath, 'PortHandles');
    try
        set(pH.Outport(signal.OutputPortIndex), 'RTWStorageClass', 'ExportedGlobal');
    catch EX
        disp(EX);
        return
    end
end

if regenModel
    % Build model
    delete([model grt_rtw '*']);
    rtwbuild(model);
end


%% Compile MATLAB functions

matLibFiles = {'rank', 'mldivide'};

if regenModel
    disp('Compiling essential MATLAB functions for trimming...');

    system('rmdir /S /Q codegen');
    linalg = 'linalg';

    codegen -config:lib -o linalg rank -args {coder.typeof(1,[50 50],[true,true])} mldivide -args {coder.typeof(1,[50 50],[1,1]), coder.typeof(1,[50,1],1)}

    copyfile(['codegen/lib/' linalg '/' linalg '_types.h'], [model grt_rtw linalg '_types.h']);
    copyfile(['codegen/lib/' linalg '/' linalg '.lib'], [model grt_rtw linalg '.lib']);
    for i=1:length(matLibFiles)
        copyfile(['codegen/lib/' linalg '/' matLibFiles{i} '.h'], [model grt_rtw matLibFiles{i} '.h']);
    end
end


%% Move files to the GRT directory

disp('Generating wrapper source files...');

CodeSource = [origDir '/CoderSource/'];
ReadData = 'read_data';
NewtonTrim = 'simulation';
Timer = 'ext_timer';
Maneuvers = 'maneuvers';
copyfile([CodeSource ReadData '.h'], [model grt_rtw ReadData '.h']);
copyfile([CodeSource ReadData '.c'], [model grt_rtw ReadData '.c']);
copyfile([CodeSource NewtonTrim '.h'], [model grt_rtw NewtonTrim '.h']);
copyfile([CodeSource NewtonTrim '.c'], [model grt_rtw NewtonTrim '.c']);
copyfile([CodeSource Timer '.h'], [model grt_rtw Timer '.h']);
copyfile([CodeSource Timer '.c'], [model grt_rtw Timer '.c']);
copyfile([CodeSource Maneuvers '.h'], [model grt_rtw Maneuvers '.h']);
copyfile([CodeSource Maneuvers '.c'], [model grt_rtw Maneuvers '.c']);


%% Input/Output/Parameters/Signals mapping

IO_mapping = 'IO_mapping';

mappingH = fopen([model grt_rtw IO_mapping '.h'], 'w');
fprintf(mappingH, '#ifndef IO_MAPPING_H\n#define IO_MAPPING_H\n');
fprintf(mappingH, '#include "%s.h"\n', model);

fprintf(mappingH, '#define MAX_IOS_SIZE 200\n');
fprintf(mappingH, '#define MAX_MANEUVER_SIZE 500\n');

fprintf(mappingH, 'void Model_SetParameter(const char* paramName, real_T paramVal);\n');
fprintf(mappingH, 'real_T Model_GetParameter(const char* paramName);\n');
fprintf(mappingH, 'real_T Model_GetSignal(const char* signalName);\n');
fprintf(mappingH, 'void Model_SetInput(const char* inputName, real_T inputVal);\n');
fprintf(mappingH, 'real_T Model_GetOutput(const char* outputName);\n');
fprintf(mappingH, 'void Model_SetState(const char* stateName, real_T stateVal);\n');
fprintf(mappingH, 'real_T Model_GetStateDerivative(const char* stateName);\n');
fprintf(mappingH, 'real_T Model_GetCurrentTime();\n');
fprintf(mappingH, 'real_T Model_GetTimeStep();\n');
fprintf(mappingH, '#endif');
fclose(mappingH);

mappingC = fopen([model grt_rtw IO_mapping '.c'], 'w');
fprintf(mappingC, '#include <stdio.h>\n');
fprintf(mappingC, '#include <string.h>\n');
fprintf(mappingC, '#include "%s.h"\n', model);
fprintf(mappingC, '#include "%s.h"\n', IO_mapping);

% Get all Simulink Parameters
fprintf(mappingC, '\nvoid Model_SetParameter(const char* paramName, real_T paramVal){\n');
simParams = Simulink.findVars(model,'SourceType','base workspace');
ind = 0;
for i=1:length(simParams)
    % Make sure this is a Simulink Parameter
    if strcmp(evalin('base',sprintf('class(%s)',simParams(i).Name)), 'Simulink.Parameter')
        ind = ind+1;
    else
        continue
    end
    
    if ind>1
        fprintf(mappingC, 'else ');
    end
    fprintf(mappingC, 'if (strcmp(paramName, "%s")==0)\n', simParams(i).Name);
    fprintf(mappingC, '%s = paramVal;\n', simParams(i).Name);
end
fprintf(mappingC, '}\n');
fprintf(mappingC, '\nreal_T Model_GetParameter(const char* paramName){\n');
ind = 0;
for i=1:length(simParams)
    % Make sure this is a Simulink Parameter
    if strcmp(evalin('base',sprintf('class(%s)',simParams(i).Name)), 'Simulink.Parameter')
        ind = ind+1;
    else
        continue
    end
    
    if ind>1
        fprintf(mappingC, 'else ');
    end
    fprintf(mappingC, 'if (strcmp(paramName, "%s")==0)\n', simParams(i).Name);
    fprintf(mappingC, 'return %s;\n', simParams(i).Name);
end
fprintf(mappingC, 'else return 0;\n');
fprintf(mappingC, '}\n');

% Get all Simulink Signals
fprintf(mappingC, '\nreal_T Model_GetSignal(const char* signalName){\n');
numSignals = simSignals.Count;
for i=1:numSignals
    signal = get(simSignals,i);
    blockPath = signal.BlockPath.getBlock(1);
    pH = get_param(blockPath, 'PortHandles');
    name = get_param(pH.Outport(signal.OutputPortIndex),'Name');
    
    if i>1
        fprintf(mappingC, 'else ');
    end
    fprintf(mappingC, 'if (strcmp(signalName, "%s")==0)\n', name);
    fprintf(mappingC, 'return %s;\n', name);
end
fprintf(mappingC, 'else { fprintf(stderr, "No signal %%s\\n", signalName); return 0; }\n');
fprintf(mappingC, '}\n');

% Get all inputs
fprintf(mappingC, '\nvoid Model_SetInput(const char* inputName, real_T inputVal){\n');
ports = find_system(model,'FindAll','On','SearchDepth',1,'BlockType','Inport'); 
inportNames = get(ports,'Name');
if iscell(inportNames)
    for i=1:length(inportNames)
        if i>1
            fprintf(mappingC, 'else ');
        end

        fprintf(mappingC, 'if (strcmp(inputName, "%s")==0)\n', inportNames{i});
        fprintf(mappingC, '%s_U.%s=inputVal;\n', model, inportNames{i});
    end
else
    fprintf(mappingC, 'if (strcmp(inputName, "%s")==0)\n', inportNames);
    fprintf(mappingC, '%s_U.%s=inputVal;\n', model, inportNames);
end
fprintf(mappingC, '}\n');

% Get all outputs
fprintf(mappingC, '\nreal_T Model_GetOutput(const char* outputName){\n');
ports = find_system(model,'FindAll','On','SearchDepth',1,'BlockType','Outport'); 
outportNames = get(ports,'Name');
if iscell(outportNames)
    for i=1:length(outportNames)
        if i>1
            fprintf(mappingC, 'else ');
        end

        fprintf(mappingC, 'if (strcmp(outputName, "%s")==0)\n', outportNames{i});
        fprintf(mappingC, 'return %s_Y.%s;\n', model, outportNames{i});
    end
else
    fprintf(mappingC, 'if (strcmp(outputName, "%s")==0)\n', outportNames);
    fprintf(mappingC, 'return %s_Y.%s;\n', model, outportNames);
end

fprintf(mappingC, 'else { fprintf(stderr,"No signal %%s\\n", outputName); return 0; }\n');
fprintf(mappingC, '}\n');

% Get all states
fprintf(mappingC, '\nvoid Model_SetState(const char* stateName, real_T stateVal){\n');
stateObjs = Simulink.BlockDiagram.getInitialState(model);
for i=1:length(stateObjs.signals)
    if i>1
        fprintf(mappingC, 'else ');
    end
    fprintf(mappingC, 'if (strcmp(stateName, "%s")==0) %s_X.%s = stateVal;\n', stateObjs.signals(i).stateName, model, stateObjs.signals(i).stateName);
end
fprintf(mappingC, '}\n');

% Get all state derivs
fprintf(mappingC, '\nreal_T Model_GetStateDerivative(const char* stateName){\n');
fprintf(mappingC, 'XDot_%s_T* Xdot = (XDot_%s_T *) %s_M->derivs;\n', model, model, model);
stateObjs = Simulink.BlockDiagram.getInitialState(model);
for i=1:length(stateObjs.signals)
    if i>1
        fprintf(mappingC, 'else ');
    end
    fprintf(mappingC, 'if (strcmp(stateName, "%s")==0) return Xdot->%s;\n', stateObjs.signals(i).stateName, stateObjs.signals(i).stateName);
end
fprintf(mappingC, 'else { fprintf(stderr,"No state %%s\\n", stateName); return 0; }\n');
fprintf(mappingC, '}\n');

% current simulation time
fprintf(mappingC, '\nreal_T Model_GetCurrentTime(){\n');
fprintf(mappingC, 'return %s_M->Timing.t[0];', model);
fprintf(mappingC, '}\n');

% step size
fprintf(mappingC, '\nreal_T Model_GetTimeStep(){\n');
fprintf(mappingC, 'return %s_M->Timing.stepSize0;', model);
fprintf(mappingC, '}\n');

fclose(mappingC);


%% Generate main file

main = [model '_main'];

main_C = fopen([model grt_rtw main '.c'], 'w');
fprintf(main_C, '#include "%s.h"\n', model);
fprintf(main_C, ['#include "' IO_mapping '.h"\n']);
fprintf(main_C, ['#include "' ReadData '.h"\n']);
fprintf(main_C, ['#include "' NewtonTrim, '.h"\n']);
fprintf(main_C, ['#include "' Timer, '.h"\n']);
fprintf(main_C, ['#include "' Maneuvers, '.h"\n']);
for i=1:length(matLibFiles)
    fprintf(main_C, ['#include "' matLibFiles{i} '.h"\n']);
end
fprintf(main_C, '#include <stdlib.h>\n');
fprintf(main_C, '#include <string.h>\n');
fprintf(main_C, '#include <stdio.h>\n\n');

% string compare
fprintf(main_C, 'static int strcmp4sort(const void *a, const void *b){ return strcmp (*(const char **) a, *(const char **) b); }\n\n');

fprintf(main_C, '\nint_T main(int_T argc, char *argv[]){\n');

% Documentation
fprintf(main_C, 'if (argc == 1) { printf("\\n%s simulation model executable\\n\\nAerodynamics: %s\\nPropulsion: %s\\n\\nUsage:\\tEXE <datafile-path> <simtime> [-trimout <trimout-filepath>] [-linout <linout-filepath>] [-simout <simout-filepath>] [-osr <output-sampling-rate-Hz>]\\n\\n"); return 0; }\n\n', model, evalin('base','Aero.ID'), evalin('base','Prop.ID'));

% Check input variables
fprintf(main_C, 'if (argc < 2) { fprintf(stderr,"No data file provided\\n"); return 99; }\n');
fprintf(main_C, 'char *dataFileName = argv[1];\n');
fprintf(main_C, 'if (argc < 3) { fprintf(stderr,"No simulation time provided\\n"); return 100; }\n');
fprintf(main_C, 'double tsim = atof(argv[2]); if (tsim < 0) { fprintf(stderr,"Simulation time must be equal or larger than 0\\n"); return 110; }\n');
fprintf(main_C, 'boolean_T trimToFile = 0; char *trimFileName = NULL;\n');
fprintf(main_C, 'boolean_T simToFile = 0; char simFileName[1000];\n');
fprintf(main_C, 'boolean_T linout = 0; char *linFileName = NULL;\n');
fprintf(main_C, 'int_T osr = 0;\n');
fprintf(main_C, 'for (int_T i=3; i<argc; i++){\n');
fprintf(main_C, '  if (strcmp(argv[i], "-trimout")==0)\n');
fprintf(main_C, '    if (i+1 < argc) { trimToFile = 1; trimFileName = argv[++i]; }\n');
fprintf(main_C, '    else { fprintf(stderr,"trimout specified but no file name provided\\n"); return 101; }\n');
fprintf(main_C, '  else if (strcmp(argv[i], "-simout")==0)\n');
fprintf(main_C, '    if (i+1 < argc) { simToFile = 1; strcpy(simFileName, argv[++i]); }\n');
fprintf(main_C, '    else { fprintf(stderr,"simout specified but no file name provided\\n"); return 102; }\n');
fprintf(main_C, '  else if (strcmp(argv[i], "-osr")==0)\n');
fprintf(main_C, '    if (i+1 < argc) { osr = atoi(argv[++i]); if (osr < 0) {fprintf(stderr,"osr must be a positive integer\\n"); return 103;} }\n');
fprintf(main_C, '    else { fprintf(stderr,"osr specified but no integer provided\\n"); return 103; }\n');
fprintf(main_C, '  else if (strcmp(argv[i], "-linout")==0)\n');
fprintf(main_C, '    if (i+1 < argc) { linout = 1; linFileName = argv[++i]; }\n');
fprintf(main_C, '    else { fprintf(stderr,"linout specified but no file name provided\\n"); return 104; }\n');
fprintf(main_C, '}\n\n');

fprintf(main_C, 'if (!simToFile) strcpy(simFileName, "%s.sim");\n\n', model);

% All the inputs
if iscell(inportNames)
    len = length(inportNames);
else
    len = 1;
end
fprintf(main_C, 'const int_T numInputs = %d; const char *inputNames[] = {', len);
if iscell(inportNames)
    for i=1:length(inportNames)
        if i>1
            fprintf(mappingC, ',');
        end

        fprintf(mappingC, '"%s"', inportNames{i});
    end
else
    fprintf(mappingC, '"%s"', inportNames);
end
fprintf(mappingC, '};\n');

% All the outputs
if iscell(outportNames)
    len = length(outportNames);
else
    len = 1;
end
fprintf(main_C, 'const int_T numOutputs = %d; const char *outputNames[] = {', len);
if iscell(outportNames)
    for i=1:length(outportNames)
        if i>1
            fprintf(mappingC, ',');
        end

        fprintf(mappingC, '"%s"', outportNames{i});
    end
else
    fprintf(mappingC, '"%s"', outportNames);
end
fprintf(mappingC, '};\n');

% All the states 
numStates = 0;
fprintf(main_C, 'const char *stateNames[] = {');
for i=1:length(stateObjs.signals)
    if i>1
        fprintf(mappingC, ",");
    end
    fprintf(mappingC, '"%s"', stateObjs.signals(i).stateName);
    numStates = numStates+1;
end
fprintf(mappingC, '}; const int_T numStates = %d; \n', numStates);

% All the parameters
fprintf(mappingC, 'const char *parameterNames[] = {');
ind = 0;
for i=1:length(simParams)
    % Make sure this is a Simulink Parameter
    if strcmp(evalin('base',sprintf('class(%s)',simParams(i).Name)), 'Simulink.Parameter')
        ind = ind+1;
    else
        continue
    end
    
    if ind>1
        fprintf(mappingC, ', ');
    end
    fprintf(mappingC, '"%s"', simParams(i).Name);
end
fprintf(mappingC, '}; const int_T numParameters = %d;\n', ind);

% All the logged signals
fprintf(mappingC, 'char *signalNames[] = {');
for i=1:numSignals
    signal = get(simSignals,i);
    blockPath = signal.BlockPath.getBlock(1);
    pH = get_param(blockPath, 'PortHandles');
    name = get_param(pH.Outport(signal.OutputPortIndex),'Name');
    
    if i>1
        fprintf(mappingC, ',');
    end
    fprintf(mappingC, ['"' name '"']);
end
fprintf(mappingC, '}; const int_T numSignals = %d;\n\n', N);
fprintf(mappingC, 'qsort(signalNames, numSignals, sizeof (const char *), strcmp4sort);\n');

%-- Trim

% Initialize model
fprintf(main_C, '%s_initialize();\n', model);

% Read data file
fprintf(main_C, 'boolean_T freezeInputs[MAX_IOS_SIZE]; for (int i=0; i<numInputs; freezeInputs[i++]=1);\n');
fprintf(main_C, 'boolean_T freezeOutputs[MAX_IOS_SIZE]; for (int i=0; i<numOutputs; freezeOutputs[i++]=0);\n');
fprintf(main_C, 'boolean_T freezeStates[MAX_IOS_SIZE]; for (int i=0; i<numStates; freezeStates[i++]=1);\n');
fprintf(main_C, 'boolean_T freezeStatesDerivs[MAX_IOS_SIZE]; for (int i=0; i<numStates; freezeStatesDerivs[i++]=0);\n');
fprintf(main_C, 'real_T inputs[MAX_IOS_SIZE]; for (int i=0; i<numInputs; inputs[i++]=0);\n');
fprintf(main_C, 'real_T outputs[MAX_IOS_SIZE]; for (int i=0; i<numOutputs; outputs[i++]=0);\n');
fprintf(main_C, 'real_T states[MAX_IOS_SIZE]; for (int i=0; i<numStates; states[i++]=0);\n');
fprintf(main_C, 'real_T statesDerivs[MAX_IOS_SIZE]; for (int i=0; i<numStates; statesDerivs[i++]=0);\n');
fprintf(main_C, 'readDataFile(dataFileName, numInputs, inputNames, numParameters, parameterNames, numOutputs, outputNames, numStates, stateNames, freezeInputs, inputs, freezeOutputs, outputs, freezeStates, states, freezeStatesDerivs, statesDerivs, NULL);\n');

fprintf(main_C, 'FILE *hTrimFile; \nif (trimToFile){\n');
fprintf(main_C, 'hTrimFile = fopen(trimFileName, "w");\n');
fprintf(main_C, 'if (hTrimFile == NULL) { fprintf(stderr, "Cannot write to %%s\\n", trimFileName); exit(1); }\n');
fprintf(main_C, '} else hTrimFile = stdout;\n');

% Perform trim
fprintf(main_C, 'IsTrim = 1;\n');
fprintf(main_C, 'char t_buffer[26]; getCurrentTime(t_buffer); fprintf(stdout, "Starting trimming...\\n"); fprintf(hTrimFile, "Trimming started at: %%s\\n\\n", t_buffer);\n');
fprintf(main_C, 'double trimStates[MAX_IOS_SIZE]; double trimInputs[MAX_IOS_SIZE];\n');
fprintf(main_C, 'int_T trimSuccess = trim(numInputs, inputNames, numOutputs, outputNames, numStates, stateNames, freezeInputs, inputs, freezeOutputs, outputs, freezeStates, states, freezeStatesDerivs, statesDerivs, &%s_step, trimStates, trimInputs);\n', model);

fprintf(main_C, 'fprintf(hTrimFile, "\\n\\n");\n');
fprintf(main_C, 'double trimSignals[%d];\n', numSignals);
fprintf(main_C, 'if (trimSuccess != -1) printTrimCase(hTrimFile, numInputs, inputNames, numOutputs, outputNames, numStates, stateNames, numSignals, (const char**)signalNames, freezeInputs, trimInputs, freezeOutputs, freezeStates, trimStates, freezeStatesDerivs, trimSignals);\n');
fprintf(main_C, 'if (trimSuccess == 0) fprintf(hTrimFile, "Trim successfully completed.\\n\\n"); else fprintf(hTrimFile, "Trim FAILED.\\n\\n");\n');

fprintf(main_C, 'if (hTrimFile != stdout) fclose(hTrimFile);\n\n');
fprintf(main_C, 'IsTrim = 0;\n');

%-- State Space Linearization

fprintf(main_C, 'if (linout && trimSuccess==0){\n');
fprintf(main_C, 'IsTrim = 1;\n');

fprintf(main_C, 'FILE *hLinFile = fopen(linFileName, "w");\n');
fprintf(main_C, 'if (hLinFile == NULL) { fprintf(stderr, "Cannot write to %%s\\n", linFileName); exit(1); }\n');

fprintf(main_C, 'linearize(hLinFile, numInputs, inputNames, numOutputs, outputNames, numStates, stateNames, trimInputs, trimStates, &%s_step);\n', model);

fprintf(main_C, 'fclose(hTrimFile);\n');
fprintf(main_C, 'IsTrim = 0;\n');
fprintf(main_C, '}\n');

%-- Sim

fprintf(main_C, 'if (tsim > 0 && trimSuccess==0){\n');

% Need to re-initialize and read the data file again
fprintf(main_C, '%s_initialize();\n', model);
fprintf(main_C, 'struct time_slice *deltaManeuvers = newPlaceHolderTimeSequence(numInputs, numParameters);\n');
fprintf(main_C, 'readDataFile(dataFileName, numInputs, inputNames, numParameters, parameterNames, numOutputs, outputNames, numStates, stateNames, freezeInputs, inputs, freezeOutputs, outputs, freezeStates, states, freezeStatesDerivs, statesDerivs, deltaManeuvers);\n');

% Set trim states and inputs
fprintf(main_C, 'for (int i=0; i<numStates; i++) Model_SetState(stateNames[i], trimStates[i]);\n');
fprintf(main_C, 'for (int i=0; i<numInputs; i++) Model_SetInput(inputNames[i], trimInputs[i]);\n');

fprintf(main_C, 'FILE *hSimFile = fopen(simFileName, "w"); \n');
fprintf(main_C, 'if (hSimFile == NULL) { fprintf(stderr, "Cannot write to %%s\\n", simFileName); freeTimeSequence(deltaManeuvers); exit(1); }\n');

% Perform simulation
fprintf(main_C, 'fprintf(stdout, "\\nStarting simulation...\\n");');
fprintf(main_C, 'fprintf(hSimFile, "Simulation: %%s\\n", t_buffer);\n');
fprintf(main_C, 'sim(hSimFile, tsim, osr, numInputs, inputNames, numParameters, parameterNames, numSignals, (const char**)signalNames, trimInputs, trimSignals, deltaManeuvers, &%s_step);\n', model);

fprintf(main_C, 'fclose(hSimFile);\n');
fprintf(main_C, 'freeTimeSequence(deltaManeuvers);\n');
fprintf(main_C, 'fprintf(stdout, "Simulation data saved to %%s.\\n", simFileName);\n\n');
fprintf(main_C, '}\n');

% clean up
fprintf(main_C, '%s_terminate();\n', model);
fprintf(main_C, '}\n');
fclose(main_C);


%% Modify model.h and .c to allow for trimming

if regenModel
    % .c
    
    model_C = fopen([model grt_rtw model '.c'], 'rt');
    model_C_tmp = fopen([model grt_rtw model '.ctmp'], 'w');
    
    tline = fgetl(model_C);
    ODEfuncSection = 0; numBrackets = 0;
    while ischar(tline)
        tline = strrep(tline, '\', '\\');
        
        if strcmp(strtrim(tline), sprintf('%s_step();', model))
            tline = sprintf('%s_step(0);',model);
        elseif strcmp(tline, 'static void rt_ertODEUpdateContinuousStates(RTWSolverInfo *si )')
            tline = 'static void rt_ertODEUpdateContinuousStates(RTWSolverInfo *si, boolean_T trimOnly)';
            ODEfuncSection = 1;
        elseif startsWith(strtrim(tline), 'rt_ertODEUpdateContinuousStates(')
            tline = sprintf('rt_ertODEUpdateContinuousStates(&%s_M->solverInfo, trimOnly);',model);
        elseif strcmp(strtrim(tline), sprintf('if (rtmIsMajorTimeStep(%s_M)) {',model))
            tline = sprintf('if (rtmIsMajorTimeStep(%s_M) || trimOnly) {',model);
        elseif strcmp(strtrim(tline), sprintf('void %s_step(void)',model))
            tline = sprintf('void %s_step(boolean_T trimOnly)',model);
        end
        
        if ODEfuncSection && ~startsWith(tline, 'static void ')
            numBrackets = numBrackets + count(tline,'{') - count(tline,'}');
            
            if ~isempty(strtrim(tline)) && numBrackets == 0
                ODEfuncSection = 0;
                tline = strcat(tline, '}');
            elseif numBrackets > 0 && startsWith(strtrim(tline), 'local_numjac(')
                tline = strcat('if (!trimOnly) {\n', tline);
            end
        end
        
        fprintf(model_C_tmp, tline); fprintf(model_C_tmp, '\n');
        
        tline = fgetl(model_C);
    end
    fclose(model_C);
    fclose(model_C_tmp);
    delete([model grt_rtw model '.c']);
    movefile([model grt_rtw model '.ctmp'], [model grt_rtw model '.c']);
    
    
    % .h
    
    model_H = fopen([model grt_rtw model '.h'], 'rt');
    model_H_tmp = fopen([model grt_rtw model '.htmp'], 'w');
    
    tline = fgetl(model_H);
    while ischar(tline)
        tline = strrep(tline, '\', '\\');
        
        if strcmp(strtrim(tline), sprintf('extern void %s_step(void);',model))
            tline = sprintf('extern void %s_step(boolean_T trimOnly);',model);
        end
        
        fprintf(model_H_tmp, tline); fprintf(model_H_tmp, '\n');
        
        tline = fgetl(model_H);
    end
    fclose(model_H);
    fclose(model_H_tmp);
    delete([model grt_rtw model '.h']);
    movefile([model grt_rtw model '.htmp'], [model grt_rtw model '.h']);
end


%% Modify makefile

if regenModel
    mk = fopen([model grt_rtw model '.mk'], 'rt');
    mk_tmp = fopen([model grt_rtw model '.mktmp'], 'w');

    tline = fgetl(mk);
    while ischar(tline)
        tline = strrep(tline, '\', '\\');
        tline = strrep(tline, '%', '%%');

        if startsWith(tline, 'OBJS = ')
            oldObjs = tline(8:end);
            tline = ['OBJS = ' IO_mapping '.obj ' ReadData '.obj ' NewtonTrim '.obj ' Timer '.obj ' Maneuvers '.obj ' oldObjs];
        elseif startsWith(tline, 'MAIN_OBJ = ')
            tline = ['MAIN_OBJ = ' main '.obj'];
        elseif startsWith(tline, 'SYSTEM_LIBS')
            tline = ['SYSTEM_LIBS = ' linalg '.lib'];
        end

        fprintf(mk_tmp, tline); fprintf(mk_tmp, '\n');
        
        tline = fgetl(mk);
    end
    fclose(mk);
    fclose(mk_tmp);
    delete([model grt_rtw model '.mk']);
    movefile([model grt_rtw model '.mktmp'], [model grt_rtw model '.mk']);
end


%% Compile and link wrapper program

disp('Generating final wrapper executable...');

system(['cd ' model grt_rtw ' & ' model ' & cd ..']);
cd(origDir);