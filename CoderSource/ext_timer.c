#include <time.h>
#include "ext_timer.h"

void getCurrentTime(char *out){
    time_t timer;
    struct tm* tm_info;
    time(&timer);
    tm_info = localtime(&timer);
    strftime(out, 26, "%Y-%m-%d %H:%M:%S", tm_info);
}