#ifndef _NEWTON_TRIM_H
#define _NEWTON_TRIM_H
#include <stdio.h>      // needed for FILE structure
#include "maneuvers.h"

/**
 * Perform Newton-Raphson trim
 * On success, return 0, all other returns are failures
 */
int trim(const int numInputs, const char *inputNames[], 
        const int numOutputs, const char *outputNames[], const int numStates,
        const char *stateNames[], 
        const unsigned char *freezeInputs, const double *inputs, const unsigned char *freezeOutputs, 
        const double *outputs, const unsigned char *freezeStates, const double *states, 
        const unsigned char *freezeStatesDerivs, const double *statesDerivs,
        void (*model_step)(unsigned char),
        double *trimStates, double *trimInputs);

/**
 * Print trim case to stdout
 * Populate trimSignals
 */
void printTrimCase(FILE *out, const int numInputs, const char *inputNames[], 
        const int numOutputs, const char *outputNames[], const int numStates,
        const char *stateNames[], const int numSignals, const char *signalNames[],
        const unsigned char *freezeInputs, const double *trimInputs, const unsigned char *freezeOutputs, 
        const unsigned char *freezeStates, const double *trimStates, 
        const unsigned char *freezeStatesDerivs, double *trimSignals);

/**
 * Simulate by time stepping
 */
void sim(FILE* out, const double tsim, const int osr, const int numInputs, const char *inputNames[], 
        const int numParameters, const char *parameterNames[],
        const int numSignals, const char *signalNames[], 
        const double *trimInputs, const double *trimSignals, struct time_slice *deltaManeuvers,
        void (*model_step)(unsigned char));

/**
 * Linearize the trimmed system
 */
void linearize(FILE* out, const int numInputs, const char *inputNames[], 
        const int numOutputs, const char *outputNames[], const int numStates,
        const char *stateNames[], const double *trimInputs, const double *trimStates,
        void (*model_step)(unsigned char));

#endif