#include "maneuvers.h"
#include <stdlib.h>
#include <math.h>

struct time_slice *newTimeSlice(const int numInputs, const int numParameters, const double t){
    struct time_slice *ts = (struct time_slice*)malloc(sizeof(struct time_slice));
    
    ts->inputs = (double *)malloc(numInputs*sizeof(double));
    for (int i=0; i<numInputs; ts->inputs[i++] = 0);
    ts->parameters = (double *)malloc(numParameters*sizeof(double));
    for (int i=0; i<numParameters; ts->parameters[i++] = 0);
    
    ts->t = t;
    ts->next = NULL;
    
    return ts;
}

void freeTimeSlice(struct time_slice *ts){
    free(ts->inputs);
    free(ts->parameters);
    free(ts);
}

struct time_slice *newPlaceHolderTimeSequence(const int numInputs, const int numParameters){
    // start node
    struct time_slice *ts0 = newTimeSlice(numInputs, numParameters, 0);
    
    // end node
    ts0->next = newTimeSlice(numInputs, numParameters, 1);
    
    return ts0;
}

void freeTimeSequence(struct time_slice *ts){
    while (ts != NULL){
        struct time_slice *ts1 = ts->next;
        freeTimeSlice(ts);
        ts = ts1;
    }
}

// assume that there are always at least 2 breakpoints
void combineTimeSequences(struct time_slice *tsA, struct time_slice *tsB, const int numInputs, const int numParameters){
    double tol = 1e-6;
    struct time_slice *origTsA = tsA;
    struct time_slice *origTsB = tsB;
    
    // the first one is always t=0
    struct time_slice *tsC = newTimeSlice(numInputs, numParameters, 0);
    struct time_slice *origTsC = tsC;
    for (int i=0; i<numInputs; i++)
        tsC->inputs[i] = tsA->inputs[i] + tsB->inputs[i];
    for (int i=0; i<numParameters; i++)
        tsC->parameters[i] = tsA->parameters[i] + tsB->parameters[i];
    
    struct time_slice *prevA = tsA;
    struct time_slice *prevB = tsB;
    while (tsC->t < tsA->t-tol || tsC->t < tsB->t-tol || tsA->next != NULL || tsB->next != NULL){
        // at this point, tsC should be sync'ed up with tsA and/or tsB
        
        // if sync'ed up with A, advance A
        if (fabs(tsC->t - tsA->t) < tol && tsA->next != NULL){
            prevA = tsA;
            tsA = tsA->next;
        }
        
        // if sync'ed up with B, advance B
        if (fabs(tsC->t - tsB->t) < tol && tsB->next != NULL){
            prevB = tsB;
            tsB = tsB->next;
        }
        
        // add block
        if ((tsC->t < tsA->t-tol) && (tsA->t <= tsB->t || tsC->t >= tsB->t)){
            tsC->next = newTimeSlice(numInputs, numParameters, tsA->t);
            tsC = tsC->next;
            for (int i=0; i<numInputs; i++)
                tsC->inputs[i] = tsA->inputs[i] + (tsB->inputs[i]-prevB->inputs[i])/(tsB->t-prevB->t)*(tsC->t-prevB->t)+prevB->inputs[i];
            for (int i=0; i<numParameters; i++)
                tsC->parameters[i] = tsA->parameters[i] + (tsB->parameters[i]-prevB->parameters[i])/(tsB->t-prevB->t)*(tsC->t-prevB->t)+prevB->parameters[i];
        } else if ((tsC->t < tsB->t-tol) && (tsB->t <= tsA->t || tsC->t >= tsA->t)){
            tsC->next = newTimeSlice(numInputs, numParameters, tsB->t);
            tsC = tsC->next; 
            for (int i=0; i<numInputs; i++)
                tsC->inputs[i] = tsB->inputs[i] + (tsA->inputs[i]-prevA->inputs[i])/(tsA->t-prevA->t)*(tsC->t-prevA->t)+prevA->inputs[i];
            for (int i=0; i<numParameters; i++)
                tsC->parameters[i] = tsB->parameters[i] + (tsA->parameters[i]-prevA->parameters[i])/(tsA->t-prevA->t)*(tsC->t-prevA->t)+prevA->parameters[i];
        }
    }
    
    // cap maneuver
    tsC->next = newTimeSlice(numInputs, numParameters, tsC->t+1);
    for (int i=0; i<numInputs; i++)
        tsC->next->inputs[i] = tsC->inputs[i];
    for (int i=0; i<numParameters; i++)
        tsC->next->parameters[i] = tsC->parameters[i];
    
    // ensure tsA has the final chain
    for (int i=0; i<numInputs; i++)
        origTsA->inputs[i] = origTsC->inputs[i];
    for (int i=0; i<numParameters; i++)
        origTsA->parameters[i] = origTsC->parameters[i];
    freeTimeSequence(origTsA->next);
    origTsA->next = origTsC->next;
    freeTimeSlice(origTsC);
}