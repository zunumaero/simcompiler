#ifndef _MANEUVERS_H
#define _MANEUVERS_H
#include "IO_mapping.h"

struct time_slice {
    double t;
    double *inputs;
    double *parameters;
    struct time_slice *next;
};

/**
 * Allocate memory for a new time_slice struct
 */
struct time_slice *newTimeSlice(const int numInputs, const int numParameters, const double t);

/**
 * Create a place holder time sequence with t=0 and t=1
 */
struct time_slice *newPlaceHolderTimeSequence(const int numInputs, const int numParameters);

/**
 * Free the memory of the entire time sequence starting at ts
 */
void freeTimeSequence(struct time_slice *ts);

/**
 * Combine tsA and tsB into tsA
 */
void combineTimeSequences(struct time_slice *tsA, struct time_slice *tsB, const int numInputs, const int numParameters);


#endif