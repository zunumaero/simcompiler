#include "simulation.h"
#include "IO_mapping.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "rank.h"
#include "mldivide.h"

#define MAX_ARR_SIZE 200

/**
 * Local function to evaluate the trimming system
 */
void getFun(const int numOutputs, const char *outputNames[], const double *outputs, 
        const unsigned char *freezeOutputs, 
        const int numStates, const char *stateNames[], const double *statesDerivs, 
        const unsigned char *freezeStatesDerivs, 
        void (*model_step)(unsigned char),
        double *fun){
    
    // run model at t=0
    (*model_step)(1);
    
    // get the evaluations
    int ind = 0;
    // frozen state derivatives
    for (int i=0; i<numStates; i++){
        if (freezeStatesDerivs[i])
            fun[ind++] = Model_GetStateDerivative(stateNames[i]) - statesDerivs[i];
    }
    // frozen outputs
    for (int i=0; i<numOutputs; i++){
        if (freezeOutputs[i])
            fun[ind++] = Model_GetOutput(outputNames[i]) - outputs[i];
    }
}


/**
 * Perform Newton-Raphson trim
 * On success, return 0, all other returns are failures
 */
int trim(const int numInputs, const char *inputNames[], 
        const int numOutputs, const char *outputNames[], const int numStates,
        const char *stateNames[], 
        const unsigned char *freezeInputs, const double *inputs, const unsigned char *freezeOutputs, 
        const double *outputs, const unsigned char *freezeStates, const double *states, 
        const unsigned char *freezeStatesDerivs, const double *statesDerivs,
        void (*model_step)(unsigned char),
        double *trimStates, double *trimInputs){
    
    const double deltaDefault = 1e-5;
    const int maxIter = 30;
    const double successTol = 1e-5;
    int status = 0;
    
    // Check num of equations and vars
    // num of eqns = freeze outputs + freeze states derivs
    // num of vars = float states + float inputs
    int numVars = 0;
    for (int i=0; i<numStates; i++){
        // copy states
        trimStates[i] = states[i];
        if (freezeStates[i]==0)
            numVars++;
    }
    for (int i=0; i<numInputs; i++){
        // copy inputs
        trimInputs[i] = inputs[i];
        if (freezeInputs[i]==0)
            numVars++;
    }
    int numEqns = 0;
    for (int i=0; i<numOutputs; i++)
        if (freezeOutputs[i]==1)
            numEqns++;
    for (int i=0; i<numStates; i++)
        if (freezeStatesDerivs[i]==1)
            numEqns++;
    if (numVars != numEqns){
        fprintf(stdout, "Indeterminate system: %d equations and %d variables.\n", numEqns, numVars);
        return -1;
    }
        
    // Perform Newton-Raphson
    double vars[MAX_ARR_SIZE]; for (int i=0; i<MAX_ARR_SIZE; vars[i++]=0);
    double varsPrev[MAX_ARR_SIZE]; memcpy(varsPrev, vars, MAX_ARR_SIZE);
    double residual = 0.0;
    int iters = 0;
    while ((iters == 0 || residual >= successTol) && iters < maxIter){
        // Evaluate function
        double fun[MAX_ARR_SIZE];
        getFun(numOutputs, outputNames, outputs, freezeOutputs, 
                numStates, stateNames, statesDerivs, freezeStatesDerivs, 
                model_step, fun);        
        
        
        // Estimate Jacobian
        int ind = 0;
        double J[MAX_ARR_SIZE*MAX_ARR_SIZE];
        // perturb states
        double fun_perturb[MAX_ARR_SIZE];
        for (int i=0; i<numStates; i++){
            if (!freezeStates[i]){
                // a floated state is a variable
                vars[ind] = trimStates[i];
                
                // get the perturbed function
                double currState = trimStates[i];
                double delta = deltaDefault;
                Model_SetState(stateNames[i], currState+delta);
                getFun(numOutputs, outputNames, outputs, freezeOutputs, 
                        numStates, stateNames, statesDerivs, freezeStatesDerivs, 
                        model_step, fun_perturb);
                
                // evaluate function derivative
                for (int j=0; j<numEqns; j++){
                    J[j+ind*numVars] = (fun_perturb[j]-fun[j])/delta;
                }
                
                // restore
                ind++;
                Model_SetState(stateNames[i], currState);
            }
        }
        // perturb inputs
        for (int i=0; i<numInputs; i++){
            if (!freezeInputs[i]){
                // a floated input is a variable
                vars[ind] = trimInputs[i];
                
                // get the perturbed function
                double currInput = trimInputs[i];
                double delta = deltaDefault;
                Model_SetInput(inputNames[i], currInput+delta);
                getFun(numOutputs, outputNames, outputs, freezeOutputs, 
                        numStates, stateNames, statesDerivs, freezeStatesDerivs, 
                        model_step, fun_perturb);
                
                // evaluate function derivative
                for (int j=0; j<numEqns; j++)
                    J[j+ind*numVars] = (fun_perturb[j]-fun[j])/delta;
                
                // restore
                ind++;
                Model_SetInput(inputNames[i], currInput);
            }
        }
        
        // check for singularity
        int sizeJ[2]; sizeJ[0]=numEqns; sizeJ[1]=numEqns;
        double r = rank(J, sizeJ);
        if ((int)round(r) < numEqns){
            fprintf(stdout, "Jacobian is singular.\n");
            status = -2;
            break;
        } else {
            // compute next states and inputs
            double varsIncr[MAX_ARR_SIZE]; int varsIncrSize[1];
            int funSize[1]; funSize[0] = numEqns;
            mldivide(J, sizeJ, fun, funSize, varsIncr, varsIncrSize);
            residual = 0;
            
            for (int i=0; i<numVars; i++){
                vars[i] = vars[i] - varsIncr[i];
                residual += pow((vars[i]-varsPrev[i]),2);
                varsPrev[i] = vars[i];
            }
            residual = sqrt(residual);
            fprintf(stdout, "Residual = %.3e\n", residual);
            
            // translate vars back into states and inputs
            ind = 0;
            for (int i=0; i<numStates; i++){
                if (!freezeStates[i]){
                    trimStates[i] = vars[ind++];
                    Model_SetState(stateNames[i], trimStates[i]);
                }
            }
            for (int i=0; i<numInputs; i++){
                if (!freezeInputs[i]){
                    trimInputs[i] = vars[ind++];
                    Model_SetInput(inputNames[i], trimInputs[i]);
                }
            }
            
            iters++;
        }
    }
    
    // terminate
    if (status < 0)
        return status;
    else if (iters >= maxIter && residual >= successTol){
        fprintf(stdout, "Max number of iterations exceeded.\n");
        return -3;
    } else {
        fprintf(stdout, "Converged.\n");
        return 0;
    }
}


/**
 * Print trim case to stdout
 */
void printTrimCase(FILE *out, const int numInputs, const char *inputNames[], 
        const int numOutputs, const char *outputNames[], const int numStates,
        const char *stateNames[], const int numSignals, const char *signalNames[],
        const unsigned char *freezeInputs, const double *trimInputs, const unsigned char *freezeOutputs, 
        const unsigned char *freezeStates, const double *trimStates, 
        const unsigned char *freezeStatesDerivs, double *trimSignals){
    
    // States
    fprintf(out, "[States]\n");
    for (int i=0; i<numStates; i++){
        fprintf(out, "%-25s%15.4f%15.4f  %-9s%-9s\n", stateNames[i], trimStates[i], 
                Model_GetStateDerivative(stateNames[i]), freezeStates[i]?"freeze":"float",
                freezeStatesDerivs[i]?"freeze":"float");
    }
    fprintf(out, "\n");
    
    // Inputs
    fprintf(out, "[Inputs]\n");
    for (int i=0; i<numInputs; i++){
        fprintf(out, "%-25s%15.4f  %-9s\n", inputNames[i], trimInputs[i], freezeInputs[i]?"freeze":"float");
    }
    fprintf(out, "\n");
    
    // Outputs
    fprintf(out, "[Outputs]\n");
    for (int i=0; i<numOutputs; i++){
        fprintf(out, "%-25s%15.4f  %-9s\n", outputNames[i], Model_GetOutput(outputNames[i]), freezeOutputs[i]?"freeze":"float");
    }
    fprintf(out, "\n");
    
    // Signals
    fprintf(out, "[Logged Signals]\n");
    for (int i=0; i<numSignals; i++){
        double val = Model_GetSignal(signalNames[i]);
        trimSignals[i] = val;
        fprintf(out, "%-25s%15.4f\n", signalNames[i], val);
    }
    fprintf(out, "\n");
}


/**
 * Private function for printing a row of signals
 */
void printSignals(FILE* out, const double t, const int numSignals, const char *signalNames[]){
    fprintf(out, "%18.4e", t);
    for (int i=0; i<numSignals; fprintf(out, "%18.4e", Model_GetSignal(signalNames[i++])));
    fprintf(out, "\n");
}


/**
 * Simulate by time stepping
 */
void sim(FILE* out, const double tsim, const int osr, const int numInputs, const char *inputNames[], 
        const int numParameters, const char *parameterNames[],
        const int numSignals, const char *signalNames[], 
        const double *trimInputs, const double *trimSignals, struct time_slice *deltaManeuvers,
        void (*model_step)(unsigned char)){
    
    unsigned int pct5 = 0;
    unsigned int pct10 = 0;
    unsigned int pct25 = 0;
    unsigned int pct50 = 0;
    unsigned int pct75 = 0;
    
    // write header
    fprintf(out, "%18s ", "t");
    for (int i=0; i<numSignals; fprintf(out, "%18s ", signalNames[i++]));
    fprintf(out, "\n");
    
    // print trim state
    fprintf(out, "%18.4e ", 0.0); 
    for (int i=0; i<numSignals; fprintf(out, "%18.4e ", trimSignals[i++]));
    fprintf(out, "\n");
    
    // update parameter maneuvers to absolute numbers
    for (int i=0; i<numParameters; i++){
        double trimParamVal = Model_GetParameter(parameterNames[i]);
        struct time_slice *currTs = deltaManeuvers;
        while (currTs != NULL){
            currTs->parameters[i] = trimParamVal + currTs->parameters[i];
            currTs = currTs->next;
        }
    }
    
    // output sampling rate
    double deltaTSample = 0.0;
    if (osr > 0)
        deltaTSample = 1.0/((double)osr);
    
    double t = 0.0; 
    long sampleCount = 0;
    while ((t=Model_GetCurrentTime()) <= tsim){
        // Retrieve inputs and parameters
        struct time_slice *currTs = deltaManeuvers; 
        while (1){
            struct time_slice *nextTs = currTs->next;

            if (t <= nextTs->t || nextTs->next == NULL){
                // interpolate/extrapolate for all signals
                for (int i=0; i<numInputs; i++){
                    double delta = (nextTs->inputs[i]-currTs->inputs[i])/(nextTs->t-currTs->t)*(t-currTs->t)+currTs->inputs[i];
                    Model_SetInput(inputNames[i], trimInputs[i]+delta);
                }
                for (int i=0; i<numParameters; i++){
                    double delta = (nextTs->parameters[i]-currTs->parameters[i])/(nextTs->t-currTs->t)*(t-currTs->t)+currTs->parameters[i];
                    Model_SetParameter(parameterNames[i], delta);
                }

                break;
            }
                    
            // move to next segment
            currTs = nextTs;
        }
        
        // time step
        (*model_step)(0);
        
        if (t >= tsim*0.05 && !pct5) {
            fprintf(stdout, "5%% completed\n");
            pct5 = 1;
        } else if (t >= tsim*0.1 && !pct10) {
            fprintf(stdout, "10%% completed\n");
            pct10 = 1;
        } else if (t >= tsim*0.25 && !pct25) {
            fprintf(stdout, "25%% completed\n");
            pct25 = 1;
        } else if (t >= tsim*0.5 && !pct50) {
            fprintf(stdout, "50%% completed\n");
            pct50 = 1;
        } else if (t >= tsim*0.75 && !pct75) {
            fprintf(stdout, "75%% completed\n");
            pct75 = 1;
        }
        
        // check for output sampling
        if (deltaTSample == 0.0 && t > 0.0 || (long)(t/deltaTSample) > sampleCount){
            double tSample;
            if (deltaTSample > 0.0) {
                sampleCount = (long)(t/deltaTSample);
                tSample = deltaTSample*((double)sampleCount);
            } else {
                tSample = t;
            }
            
            printSignals(out, tSample, numSignals, signalNames);
        }
    }
    
    fprintf(stdout, "100%% completed\n");
}


/**
 * Linearize the trimmed system
 */
void linearize(FILE* out, const int numInputs, const char *inputNames[], 
        const int numOutputs, const char *outputNames[], const int numStates,
        const char *stateNames[], const double *trimInputs, const double *trimStates,
        void (*model_step)(unsigned char)){
    
    double trimOutputs[MAX_ARR_SIZE]; for (int i=0; i<MAX_ARR_SIZE; trimOutputs[i++]=0);
    double trimStateDerivs[MAX_ARR_SIZE]; for (int i=0; i<MAX_ARR_SIZE; trimStateDerivs[i++]=0);
    const double delta = 1e-5;
    fprintf(out, "Linearized State Space\n");
    
    // print states
    fprintf(out, "\nStates X:\n");
    for (int i=0; i<numStates; i++){
        fprintf(out, "(%d) %s = %.4f\n", i, stateNames[i], trimStates[i]);
        Model_SetState(stateNames[i], trimStates[i]);
    }
    
    // print inputs
    fprintf(out, "\nInputs U:\n");
    for (int i=0; i<numInputs; i++){
        fprintf(out, "(%d) %s = %.4f\n", i, inputNames[i], trimInputs[i]);
        Model_SetInput(inputNames[i], trimInputs[i]);
    }
    
    // print outputs
    (*model_step)(1);
    fprintf(out, "\nOutputs Y:\n");
    for (int i=0; i<numOutputs; i++){
        double output = Model_GetOutput(outputNames[i]);
        fprintf(out, "(%d) %s = %.4f\n", i, outputNames[i], output);
        trimOutputs[i] = output;
    }
    
    // state derivatives
    for (int i=0; i<numStates; i++)
        trimStateDerivs[i] = Model_GetStateDerivative(stateNames[i]);
    
    // obtain A and C matrix
    double A[MAX_ARR_SIZE][MAX_ARR_SIZE];
    double C[MAX_ARR_SIZE][MAX_ARR_SIZE];
    for (int j=0; j<numStates; j++){
        Model_SetState(stateNames[j], trimStates[j]+delta);
        (*model_step)(1);
        for (int i=0; i<numStates; i++){
            A[i][j] = (Model_GetStateDerivative(stateNames[i])-trimStateDerivs[i])/delta;
        }
        for (int i=0; i<numOutputs; i++){
            C[i][j] = (Model_GetOutput(outputNames[i])-trimOutputs[i])/delta;
        }
        
        // restore
        Model_SetState(stateNames[j], trimStates[j]);
    }
    
    // obtain B and D matrix
    double B[MAX_ARR_SIZE][MAX_ARR_SIZE];
    double D[MAX_ARR_SIZE][MAX_ARR_SIZE];
    for (int j=0; j<numInputs; j++){
        Model_SetInput(inputNames[j], trimInputs[j]+delta);
        (*model_step)(1);
        for (int i=0; i<numStates; i++){
            B[i][j] = (Model_GetStateDerivative(stateNames[i])-trimStateDerivs[i])/delta;
        }
        for (int i=0; i<numOutputs; i++){
            D[i][j] = (Model_GetOutput(outputNames[i])-trimOutputs[i])/delta;
        }
        
        // restore
        Model_SetInput(inputNames[j], trimInputs[j]);
    }
    
    // print the matrices
    fprintf(out, "\nA = \n");
    for (int i=0; i<numStates; i++){
        for (int j=0; j<numStates; j++){
            fprintf(out, "%12.3e", A[i][j]);
        }
        fprintf(out, "\n");
    }
    fprintf(out, "\nB = \n");
    for (int i=0; i<numStates; i++){
        for (int j=0; j<numInputs; j++){
            fprintf(out, "%12.3e", B[i][j]);
        }
        fprintf(out, "\n");
    }
    fprintf(out, "\nC = \n");
    for (int i=0; i<numOutputs; i++){
        for (int j=0; j<numStates; j++){
            fprintf(out, "%12.3e", C[i][j]);
        }
        fprintf(out, "\n");
    }
    fprintf(out, "\nD = \n");
    for (int i=0; i<numOutputs; i++){
        for (int j=0; j<numInputs; j++){
            fprintf(out, "%12.3e", D[i][j]);
        }
        fprintf(out, "\n");
    }
}