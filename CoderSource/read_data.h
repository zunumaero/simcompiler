#ifndef _READ_DATA_H
#define _READ_DATA_H
#include <stdio.h>      // needed for FILE structure
#include "maneuvers.h"

/**
 * Read the data file and parse information to be used for trim and sim
 */
void readDataFile(const char fileName[], const int numInputs, const char *inputNames[], 
        const int numParameters, const char *parameterNames[],
        const int numOutputs, const char *outputNames[], const int numStates,
        const char *stateNames[], unsigned char *freezeInputs, double *inputs, 
        unsigned char *freezeOutputs, double *outputs, unsigned char *freezeStates,
        double *states, unsigned char *freezeStatesDerivs, double *statesDerivs,
        struct time_slice *deltaManeuvers);

/**
 * Pointer safe way of trimming the white spaces leading/trailing the string
 */
char *strtrim(char *str);

/**
 * Get a line from fid
 * @return 0 = line obtained, 1 = EOF reached
 */
int fgetl(FILE *fid, char *tline);

/**
 * Check if string starts with pre
 */
unsigned char startsWith(const char *str, const char *pre);

/**
 * Extract between start and end characters
 *@return 1 upon successful extraction
 */
unsigned char extractBetween(const char *str, const char start, const char end, char *extract);

#endif