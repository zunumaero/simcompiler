#include "read_data.h"
#include "IO_mapping.h"
#include "maneuvers.h"
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

#define MAX_NAME_SIZE 255
#define MAX_LINE_SIZE 1000

/**
 * Get a line from fid
 * @return 0 = line obtained, 1 = EOF reached
 */
int fgetl(FILE *fid, char *tline){
    int ind = 0; char c;
    do {
        c = (char)fgetc(fid);
        if (c == EOF){
            tline[ind] = '\0';
            return 1;
        } else if (c == '\n'){
            tline[ind] = '\0';
            return 0;
        } else
            tline[ind++] = c;
    } while (1);
}


/**
 * Pointer safe way of trimming the white spaces leading/trailing the string
 */
char *strtrim(char *str)
{
    size_t len = 0;
    char *frontp = str;
    char *endp = NULL;

    if( str == NULL ) { return NULL; }
    if( str[0] == '\0' ) { return str; }

    len = strlen(str);
    endp = str + len;

    /* Move the front and back pointers to address the first non-whitespace
     * characters from each end.
     */
    while( isspace((unsigned char) *frontp) ) { ++frontp; }
    if( endp != frontp )
    {
        while( isspace((unsigned char) *(--endp)) && endp != frontp ) {}
    }

    if( str + len - 1 != endp )
        *(endp + 1) = '\0';
    else if( frontp != str &&  endp == frontp )
        *str = '\0';

    /* Shift the string so that it starts at str so that if it's dynamically
     * allocated, we can still free it on the returned pointer.  Note the reuse
     * of endp to mean the front of the string buffer now.
     */
    endp = str;
    if( frontp != str )
    {
        while( *frontp ) { *endp++ = *frontp++; }
        *endp = '\0';
    }

    return str;
}


/**
 * Check if string starts with pre
 */
unsigned char startsWith(const char *str, const char *pre)
{
    size_t lenpre = strlen(pre),
           lenstr = strlen(str);
    return lenstr < lenpre ? false : strncmp(pre, str, lenpre) == 0;
}


/**
 * Extract between start and end characters
 *@return 1 upon successful extraction
 */
unsigned char extractBetween(const char *str, const char start, const char end, char *extract){
    size_t len = strlen(str);
    unsigned char in = 0; unsigned char valid = 0;
    int ind = 0;
    for (int i=0; i<len; i++){
        if (str[i] == start)
            in = 1;
        else if (in && str[i] != end)
            extract[ind++] = str[i];
        else if (in && str[i] == end){
            valid = 1;
            break;
        }
    }
    extract[ind] = '\0';
    return valid;
}


/**
 * Local function for checking whether the keyword is freeze or float
 */
int isFreeze(const char str[]){
    if (strcmp(str,"freeze")==0 || strcmp(str,"Freeze")==0)
        return 1;
    else if (strcmp(str,"float")==0 || strcmp(str,"Float")==0)
        return 0;
    else
        return -1;
}



/**
 * Read the data file and parse information to be used for trim and sim
 */
void readDataFile(const char fileName[], const int numInputs, const char *inputNames[], 
        const int numParameters, const char *parameterNames[],
        const int numOutputs, const char *outputNames[], const int numStates,
        const char *stateNames[], unsigned char *freezeInputs, double *inputs, 
        unsigned char *freezeOutputs, double *outputs, unsigned char *freezeStates,
        double *states, unsigned char *freezeStatesDerivs, double *statesDerivs,
        struct time_slice *deltaManeuvers) {
    
    char maneuverHeaders[MAX_MANEUVER_SIZE][MAX_NAME_SIZE];
    char tline[MAX_LINE_SIZE];
    struct time_slice *newDeltaManeuvers = NULL; 
    struct time_slice *currNewDeltaManeuvers = NULL;
    
    // Open file
    FILE *fid = fopen(fileName, "r");
    if (fid == NULL){
        fprintf(stderr, "Cannot open %s\n", fileName);
        freeTimeSequence(deltaManeuvers); 
        freeTimeSequence(newDeltaManeuvers); 
        exit(1);
    }
    
    // Parse file content
    char section[20] = "";
    int lineNum = 0;
    int fgetlRes = 0;
    do {
        fgetlRes = fgetl(fid, tline);
        lineNum++;
        strtrim(tline);
        
        // ignore comments and empty lines
        if (strcmp(tline,"")!=0 && !startsWith(tline, "#")){
            // replace \t with spaces
            char *tabPos = strstr(tline, "\t");
            while (tabPos != NULL){
                *tabPos = ' ';
                tabPos = strstr(tabPos, "\t");
            }
            
            if (startsWith(tline, "[")){
                if (!extractBetween(tline, '[', ']', section)){
                    fprintf(stderr, "tline %d: [ must end with ] for section definition\n", lineNum);
                    freeTimeSequence(deltaManeuvers); 
                    freeTimeSequence(newDeltaManeuvers);
                    exit(1);
                }
                
                // if there are maneuvers, combine
                if (deltaManeuvers != NULL && newDeltaManeuvers != NULL){
                    // cap maneuvers
                    currNewDeltaManeuvers->next = newTimeSlice(numInputs, numParameters, currNewDeltaManeuvers->t+1);
                    for (int i=0; i<numInputs; i++)
                        currNewDeltaManeuvers->next->inputs[i] = currNewDeltaManeuvers->inputs[i];
                    for (int i=0; i<numParameters; i++)
                        currNewDeltaManeuvers->next->parameters[i] = currNewDeltaManeuvers->parameters[i];
                    
                    if (currNewDeltaManeuvers == newDeltaManeuvers){
                        fprintf(stderr, "There must be at least two breakpoints in maneuvers\n");
                        freeTimeSequence(deltaManeuvers); 
                        freeTimeSequence(newDeltaManeuvers);
                        exit(1);
                    } else if (currNewDeltaManeuvers == NULL && newDeltaManeuvers != NULL){
                        fprintf(stderr, "No maneuvers specified after header row\n");
                        freeTimeSequence(deltaManeuvers); 
                        freeTimeSequence(newDeltaManeuvers);
                        exit(1);
                    } else {
                        combineTimeSequences(deltaManeuvers, newDeltaManeuvers, numInputs, numParameters);
                        freeTimeSequence(newDeltaManeuvers);
                        newDeltaManeuvers = NULL;
                    }
                }
                
                // reset maneuver headers
                for (int i=0; i<MAX_MANEUVER_SIZE; maneuverHeaders[i++][0]='\0');
            } else {
                
                if (strcmp(section, "Parameters")==0){
                    
                    char paramName[MAX_NAME_SIZE];
                    double paramVal = 0;
                    
                    char *token = strtok(tline, "=");
                    int ind = 0;
                    while (token != NULL){
                        strtrim(token);
                        if (startsWith(token, "#"))
                            break;
                        else {
                            switch(ind){
                                case 0:
                                    strcpy(paramName, token);
                                    break;
                                case 1:
                                    paramVal = atof(token);
                                    break;
                            }
                            
                            ind++;
                        }
                            
                        token = strtok(NULL, " ");
                    }
                    
                    if (ind != 2){
                        fprintf(stderr, "Line %d: Not properly formatted parameter specification\n", lineNum);
                        freeTimeSequence(deltaManeuvers); 
                        freeTimeSequence(newDeltaManeuvers);
                        exit(1);
                    }
                    
                    // Change model parameter
                    Model_SetParameter(paramName, paramVal);
                    
                } else if (strcmp(section, "States")==0){
                    
                    char stateName[MAX_NAME_SIZE];
                    double stateVal = 0;
                    double stateDerivVal = 0;
                    int stateFreeze = 0;
                    int stateDerivFreeze = 0;
                    
                    char *token = strtok(tline, " ");
                    int ind = 0;
                    while (token != NULL){
                        strtrim(token);
                        if (startsWith(token, "#"))
                            break;
                        else if (strcmp(token,"")!=0){
                            switch(ind){
                                case 0:
                                    strcpy(stateName, token);
                                    break;
                                case 1:
                                    stateVal = atof(token);
                                    break;
                                case 2:
                                    stateDerivVal = atof(token);
                                    break;
                                case 3:
                                    stateFreeze = isFreeze(token);
                                    if (stateFreeze == -1){
                                        fprintf(stderr, "Line %d: State must be freeze or float\n", lineNum);
                                        freeTimeSequence(deltaManeuvers); 
                                        freeTimeSequence(newDeltaManeuvers);
                                        exit(1);
                                    }
                                    break;
                                case 4:
                                   stateDerivFreeze = isFreeze(token);
                                    if (stateDerivFreeze == -1){
                                        fprintf(stderr, "Line %d: State must be freeze or float\n", lineNum);
                                        freeTimeSequence(deltaManeuvers); 
                                        freeTimeSequence(newDeltaManeuvers);
                                        exit(1);
                                    }
                                    break; 
                            }
                            
                            ind++;
                        }
                            
                        token = strtok(NULL, " ");
                    }
                    
                    if (ind != 5){
                        fprintf(stderr, "Line %d: Not properly formatted state specification %d\n", lineNum, ind);
                        freeTimeSequence(deltaManeuvers); 
                        freeTimeSequence(newDeltaManeuvers);
                        exit(1);
                    }
                    
                    // Find location
                    ind = -1;
                    for (int i=0; i<numStates; i++){
                        if (strcmp(stateName, stateNames[i])==0){
                            ind = i;
                            break;
                        }
                    }
                    if (ind == -1){
                        fprintf(stderr, "Line %d: Unrecognized state signal\n", lineNum);
                        freeTimeSequence(deltaManeuvers); 
                        freeTimeSequence(newDeltaManeuvers);
                        exit(1);
                    }

                    // Final results
                    states[ind] = stateVal;
                    Model_SetState(stateName, stateVal);
                    statesDerivs[ind] = stateDerivVal;
                    if (stateFreeze)
                        freezeStates[ind] = 1;
                    else
                        freezeStates[ind] = 0;
                    if (stateDerivFreeze)
                        freezeStatesDerivs[ind] = 1;
                    else
                        freezeStatesDerivs[ind] = 0;
                    
                } else if (strcmp(section, "Inputs")==0){
                    
                    char inputName[MAX_NAME_SIZE];
                    double inputVal = 0;
                    int inputFreeze = 0;
                    
                    char *token = strtok(tline, " ");
                    int ind = 0;
                    while (token != NULL){
                        strtrim(token);
                        if (startsWith(token, "#"))
                            break;
                        else if (strcmp(token,"")!=0){
                            switch(ind){
                                case 0:
                                    strcpy(inputName, token);
                                    break;
                                case 1:
                                    inputVal = atof(token);
                                    break;
                                case 2:
                                    inputFreeze = isFreeze(token);
                                    if (inputFreeze == -1){
                                        fprintf(stderr, "Line %d: Input variable must be freeze or float\n", lineNum);
                                        freeTimeSequence(deltaManeuvers); 
                                        freeTimeSequence(newDeltaManeuvers);
                                        exit(1);
                                    }
                                    break;
                            }
                            
                            ind++;
                        }
                            
                        token = strtok(NULL, " ");
                    }
                    
                    if (ind != 3){
                        fprintf(stderr, "Line %d: Not properly formatted input specification\n", lineNum);
                        freeTimeSequence(deltaManeuvers); 
                        freeTimeSequence(newDeltaManeuvers);
                        exit(1);
                    }
                    
                    // Find location
                    ind = -1;
                    for (int i=0; i<numInputs; i++){
                        if (strcmp(inputName, inputNames[i])==0){
                            ind = i;
                            break;
                        }
                    }
                    if (ind == -1){
                        fprintf(stderr, "Line %d: Unrecognized input signal\n", lineNum);
                        freeTimeSequence(deltaManeuvers); 
                        freeTimeSequence(newDeltaManeuvers);
                        exit(1);
                    }

                    // Final results
                    inputs[ind] = inputVal;
                    Model_SetInput(inputName, inputVal);
                    if (inputFreeze)
                        freezeInputs[ind] = 1;
                    else
                        freezeInputs[ind] = 0;
                    
                } else if (strcmp(section, "Outputs")==0){
                    
                    char outputName[MAX_NAME_SIZE];
                    double outputVal = 0;
                    int outputFreeze = 0;
                    
                    char *token = strtok(tline, " ");
                    int ind = 0;
                    while (token != NULL){
                        strtrim(token);
                        if (startsWith(token, "#"))
                            break;
                        else if (strcmp(token,"")!=0){
                            switch(ind){
                                case 0:
                                    strcpy(outputName, token);
                                    break;
                                case 1:
                                    outputVal = atof(token);
                                    break;
                                case 2:
                                    outputFreeze = isFreeze(token);
                                    if (outputFreeze == -1){
                                        fprintf(stderr, "Line %d: Output variable must be freeze or float\n", lineNum);
                                        freeTimeSequence(deltaManeuvers); 
                                        freeTimeSequence(newDeltaManeuvers);
                                        exit(1);
                                    }
                                    break;
                            }
                            
                            ind++;
                        }
                            
                        token = strtok(NULL, " ");
                    }
                    
                    if (ind != 3){
                        fprintf(stderr, "Line %d: Not properly formatted output specification\n", lineNum);
                        freeTimeSequence(deltaManeuvers); 
                        freeTimeSequence(newDeltaManeuvers);
                        exit(1);
                    }
                    
                    // Find location
                    ind = -1;
                    for (int i=0; i<numOutputs; i++){
                        if (strcmp(outputName, outputNames[i])==0){
                            ind = i;
                            break;
                        }
                    }
                    if (ind == -1){
                        fprintf(stderr, "Line %d: Unrecognized output signal\n", lineNum);
                        freeTimeSequence(deltaManeuvers); 
                        freeTimeSequence(newDeltaManeuvers);
                        exit(1);
                    }

                    // Final results
                    outputs[ind] = outputVal;
                    if (outputFreeze)
                        freezeOutputs[ind] = 1;
                    else
                        freezeOutputs[ind] = 0;
                    
                } else if (strcmp(section, "Include")==0){
                    
                    // Recursively include any other definition files
                    readDataFile(tline, numInputs, inputNames, numParameters, parameterNames,
                            numOutputs, outputNames, numStates, stateNames, freezeInputs, 
                            inputs, freezeOutputs, outputs, freezeStates, states, 
                            freezeStatesDerivs, statesDerivs, deltaManeuvers);
                    
                } else if (strcmp(section, "Maneuver")==0 && deltaManeuvers != NULL){
                    
                    if (strcmp(maneuverHeaders[0],"")==0){
                        
                        // this is a header line
                        
                        char *token = strtok(tline, " ");
                        int ind = 0;
                        while (token != NULL){
                            strtrim(token);
                            if (startsWith(token, "#"))
                                break;
                            else if (strcmp(token,"")!=0){
                                if (strcmp(token,"t")==0)
                                    // time column
                                    strcpy(maneuverHeaders[ind++], token);
                                else{
                                    // input column
                                    if (ind == 0){
                                        fprintf(stderr, "Line %d: Maneuver must begin with t\n", lineNum);
                                        freeTimeSequence(deltaManeuvers); 
                                        freeTimeSequence(newDeltaManeuvers);
                                        exit(1);
                                    } else
                                        strcpy(maneuverHeaders[ind++], token);
                                }
                            }

                            token = strtok(NULL, " ");
                        }
                        
                        newDeltaManeuvers = newTimeSlice(numInputs, numParameters, 0);
                        currNewDeltaManeuvers = NULL;
                        
                    } else {
                        
                        // translate to full line
                        
                        struct time_slice *prevTs;
                        if (currNewDeltaManeuvers == NULL){
                            // this is the very first breakpoint
                            prevTs = newDeltaManeuvers;
                            currNewDeltaManeuvers = newDeltaManeuvers;
                        } else {
                            // this is a subsequent breakpoint
                            currNewDeltaManeuvers->next = newTimeSlice(numInputs, numParameters, 0);
                            prevTs = currNewDeltaManeuvers;
                            currNewDeltaManeuvers = currNewDeltaManeuvers->next;
                        }
                        
                        char *token = strtok(tline, " ");
                        int ind = 0;
                        while (token != NULL){
                            strtrim(token);
                            if (startsWith(token, "#"))
                                break;
                            else if (strcmp(token,"")!=0){
                                if (strcmp(maneuverHeaders[ind],"t")==0){
                                    // time column
                                    currNewDeltaManeuvers->t = atof(token);
                                    if (currNewDeltaManeuvers == prevTs && currNewDeltaManeuvers->t != 0.0){
                                        fprintf(stderr, "The first maneuvers must start at t=0\n");
                                        freeTimeSequence(deltaManeuvers); 
                                        freeTimeSequence(newDeltaManeuvers);
                                        exit(1);
                                    } else if (currNewDeltaManeuvers != prevTs && currNewDeltaManeuvers->t <= prevTs->t){
                                        fprintf(stderr, "The maneuvers must be strictly increasing in time\n");
                                        freeTimeSequence(deltaManeuvers); 
                                        freeTimeSequence(newDeltaManeuvers);
                                        exit(1);
                                    }
                                } else {
                                    // input column
                                    
                                    if (strcmp(maneuverHeaders[ind],"")==0){
                                        fprintf(stderr, "Number of columns in maneuver exceeded the number of maneuver inputs\n");
                                        freeTimeSequence(deltaManeuvers); 
                                        freeTimeSequence(newDeltaManeuvers);
                                        exit(1);
                                    } else {
                                        int inputInd = -1;
                                        for (int i=0; i<numInputs; i++){
                                            if (strcmp(maneuverHeaders[ind],inputNames[i])==0){
                                                inputInd = i;
                                                break;
                                            }
                                        }
                                        int parameterInd = -1;
                                        if (inputInd == -1){
                                            for (int i=0; i<numParameters; i++){
                                                if (strcmp(maneuverHeaders[ind],parameterNames[i])==0){
                                                    parameterInd = i;
                                                    break;
                                                }
                                            }
                                        }
                                        
                                        if (inputInd == -1 && parameterInd == -1){
                                            fprintf(stderr, "Invalid input/parameter name: %s\n", maneuverHeaders[ind]);
                                            freeTimeSequence(deltaManeuvers); 
                                            freeTimeSequence(newDeltaManeuvers);
                                            exit(1);
                                        } else if (inputInd > -1)
                                            currNewDeltaManeuvers->inputs[inputInd] = atof(token);
                                        else
                                            currNewDeltaManeuvers->parameters[parameterInd] = atof(token);
                                    }
                                }
                                
                                ind++;
                            }

                            token = strtok(NULL, " ");
                        }
                        
                        // check we have the correct number of columns
                        if (strcmp(maneuverHeaders[ind],"")!=0){
                            fprintf(stderr, "Not enough columns specified in maneuvers\n");
                            freeTimeSequence(deltaManeuvers); 
                            freeTimeSequence(newDeltaManeuvers);
                            exit(1);
                        }
                    }
                    
                } // end of all sections
            }
        }
    } while (fgetlRes != 1); // EOF
    
    // if there are maneuvers, combine
    if (deltaManeuvers != NULL && newDeltaManeuvers != NULL){
        // cap maneuvers
        currNewDeltaManeuvers->next = newTimeSlice(numInputs, numParameters, currNewDeltaManeuvers->t+1);
        for (int i=0; i<numInputs; i++)
            currNewDeltaManeuvers->next->inputs[i] = currNewDeltaManeuvers->inputs[i];
        for (int i=0; i<numParameters; i++)
            currNewDeltaManeuvers->next->parameters[i] = currNewDeltaManeuvers->parameters[i];
        
        if (currNewDeltaManeuvers == newDeltaManeuvers){
            fprintf(stderr, "There must be at least two breakpoints in maneuvers\n");
            freeTimeSequence(deltaManeuvers); 
            freeTimeSequence(newDeltaManeuvers);
            exit(1);
        } else if (currNewDeltaManeuvers == NULL && newDeltaManeuvers != NULL){
            fprintf(stderr, "No maneuvers specified after header row\n");
            freeTimeSequence(deltaManeuvers); 
            freeTimeSequence(newDeltaManeuvers);
            exit(1);
        } else {
            combineTimeSequences(deltaManeuvers, newDeltaManeuvers, numInputs, numParameters);
            freeTimeSequence(newDeltaManeuvers);
            newDeltaManeuvers = NULL;
        }
    }
    
    fclose(fid);
}
