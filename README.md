# SimCompiler #

SimCompiler is designed to generate a Windows executable from a MATLAB/Simulink based aircraft model. The exe expedites the trim point calculation and simulation. The trim point specifications and simulation maneuvers are provided via a text-based data file; the trim/simulation outputs are returned as text-based displays or, optionally, files. The executable must be started from the Windows command prompt or Powershell.

The compilation requires the Simulink Compiler toolbox and the corresponding compatible MinGW-w64 codec. The compilation has been verified on MATLAB 2017a and 2017b. It's a known issue that the final linker does not work with MATLAB 2018b.

While the compiler is designed to be as generic and flexible as possible, the following rules must be obeyed in the model:

1. Init.m script initializes the model variables.

2. A Simulink Parameter named IsTrim, with ExportedGlobal scope, must be present in the model and not pruned during auto-coding. To avoid auto-coding pruning a branch, you can log a signal downstream.

3. Two structure variables must be set up in the workspace during initialization:

    - Aero: Aero.ID field contains a unique identifier for the aerodynamics model.
	
    - Prop: Prop.ID field contains a unique identifier for the propulsion model.

	
## How to Compile Model ##

In MATLAB console, execute:

    CompileModel(model_dir, model, 1)
	
model_dir is a string for the directory containing the model.

model is a string for the name of the top level model file.
	

## How to Run trim ##

Once the command prompt is at the directory containing the exe, the syntax to obtain a trim is:

	EXE_NAME <path-to-trim.dat> 0 [-trimout <path-to-trimout.trim]
	
EXE_NAME is the name of the exe.

<path-to-trim.dat> is the path to the data file (e.g. Trims/TrimFltDyn_CoordTurn.dat).

-trimout is optional. If not specified, the trim results will be printed to the console window. If specified, they will be saved to the specified file in <path-to-trimout.trim>.


## How to Run Sim ##

Once the command prompt is at the directory containing the exe, the syntax to obtain a trim is:

	EXE_NAME <path-to-trim.dat> <simtime> [-trimout <path-to-trimout.trim] [-simout <path-to-simout.sim]


simtime is the total simulation time in seconds. If 0, then this becomes a trim.

-simout is optional. If not specified, the simulation output time series will be saved to FltDyn_mr.sim. If specified, they will be saved to <path-to-simout.sim>.

All other parameters refer to the How to Run Trim section.
